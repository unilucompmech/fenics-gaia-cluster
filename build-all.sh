#!/bin/bash
set -e

source env-build-fenics.sh

./build-petsc.sh
./build-slepc.sh
./build-eigen.sh
./build-pybind11.sh
./build-python-modules.sh
./build-fenics.sh
